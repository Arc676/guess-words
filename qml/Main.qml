/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'guess-words.bhdouglass'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property string lang: i18n.language.replace('.UTF-8', '')

    property QtObject colors: QtObject {
        readonly property color divider: UbuntuColors.porcelain
        readonly property color text: UbuntuColors.porcelain
        readonly property color background: '#0E8420'
    }

    PageStack {
        id: pageStack

        Component.onCompleted: push(Qt.resolvedUrl('CategoryListPage.qml'))
    }
}
