/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0

import "loader.js" as Loader
import "languages.js" as Languages

Page {
    id: categoryLanguagePage

    property string category
    property string categoryTitle
    property var categoryData

    Component.onCompleted: {
        var languages = [];

        Object.keys(categoryData).forEach(function(lang) {
            var data = categoryData[lang];
            data.lang = lang;
            data.languageTitle = Languages.languages[lang];

            languages.push(data);
        });

        languages.sort(function(a, b) {
            if (a.lang == root.lang) {
                return -1;
            }

            if (b.lang == root.lang) {
                return 1;
            }

            if (a.languageTitle > b.languageTitle) {
                return 1;
            }

            if (a.languageTitle < b.languageTitle) {
                return -1;
            }

            return 0;
        });

        languages.forEach(function(languageData) {
            languageModel.append(languageData);
        });
    }

    header: PageHeader {
        id: header
        title: categoryTitle

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                }
            ]
        }

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }
    }

    ListModel {
        id: languageModel
    }

    Label {
        id: label
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            margins: units.gu(1)
        }

        text: i18n.tr('Choose a language')
        horizontalAlignment: Label.AlignHCenter
    }

    Flickable {
        anchors {
            top: label.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        ListView {
            anchors.fill: parent
            model: languageModel

            delegate: ListItem {
                height: layout.height

                ListItemLayout {
                    id: layout
                    title.text: languageTitle
                    title.font.bold: lang == root.lang

                    ProgressionSlot {}
                }

                onClicked: {
                    pageStack.pop();
                    pageStack.push(Qt.resolvedUrl('GamePage.qml'), {
                        category: lang + '/' + category,
                    });
                }
            }
        }
    }

    HowToPlayDialog {
        id: howToPlayDialog
    }
}
